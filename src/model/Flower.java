package model;

public class Flower {
	private int id;
	private String name;
	private int price;

	public Flower() {
		super();
		id = 0;
		name = "";
		price = 0;
	}

	public Flower(int id, String name, int price) 
	{
		super();
		this.id = id;
		this.name = name;
		this.price = price ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String toString() {
		return "Flower [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
}
