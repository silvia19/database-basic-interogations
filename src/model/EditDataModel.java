package model;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import javax.swing.JButton;

public class EditDataModel extends DefaultTableModel {
	private static final long serialVersionUID = 1L;
	final int ColumnsNumber = 4;
	final int EditColumn = 3;
	private List<Flower> flowers;

	public EditDataModel(List<Flower> list) {
		flowers = list;
	}

	public List<Flower> getflowers() {
		return flowers;
	}

	public void removeById(int id) {

		for (int i = flowers.size() - 1; i >= 0; i--) {
			Flower s = flowers.get(i);
			if (s.getId() == id)
				flowers.remove(i);
		}
		this.fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		if (flowers != null) {
			return flowers.size();
		} else {
			return 0;
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int columnIndex) {
		if(columnIndex < 4) {
			switch (columnIndex) {
				case 0: {
					return "Id";
				}
				case 1: {
					return "Name";
				}
				case 2: {
					return "Price";
				}
			}
		}
		return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0 || columnIndex == 2)
			return Integer.class;
		if (columnIndex == 3) {
			return JButton.class;
		}
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return EditColumn == columnIndex;
		//return columnIndex = 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= flowers.size())
			return null;
		Flower flower = flowers.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return flower.getId();
		case 1:
			return flower.getName();
		case 2:
			return flower.getPrice();
		}
		return null;

//		Flower flower = flowers.get(rowIndex);
//		return flower.GetAttribute(columnIndex);
	}
}
