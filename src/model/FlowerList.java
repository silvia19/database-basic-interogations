package model;

import java.util.ArrayList;
import java.util.List;

public class FlowerList {
	public ArrayList<Flower> flowerList;

	public FlowerList() {
		flowerList = new ArrayList<Flower>();
	}

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	public int getSize_flowerList() {
		return flowerList.size();
	}

	public Flower getFlowerAt(int index) {
		if (index < 0 || index >= flowerList.size()) {
			return null;
		}
		return flowerList.get(index);
	}

	public boolean addFlower(Flower flower) {
		if (flower == null) {
			return false;
		}
		return flowerList.add(flower);
	}
}
