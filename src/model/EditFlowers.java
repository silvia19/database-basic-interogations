package model;

import java.util.List;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

public class EditFlowers extends DefaultTableModel {
	private static final long serialVersionUID = 1L;
	private List<Flower> flowerList = null;

	public EditFlowers(List<Flower> mediu) {
		super();
		flowerList = mediu;
	}

	public void setValueAt(Object aValue, int row, int column) {
		Flower flower = flowerList.get(row);
		switch (column) {
		case 0: {
			flower.setId((int) aValue);
			break;
		}
		case 1: {
			flower.setName((String) aValue);
			break;
		}
		case 2: {
			flower.setPrice((int) aValue);
			break;
		}
		default:
			break;
		}
	}

	public int getRowCount() {
		if (flowerList != null) {
			return flowerList.size();
		} else {
			return 0;
		}
	}

	public int getColumnCount() {
		return 4;
	}

	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return "Id";
		case 1:
			return "Name";
		case 2:
			return "Price";
		}
		return "";
	}

	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0 || columnIndex == 2)
			return Integer.class;
		if (columnIndex == 3) {
			return JButton.class;
		}
		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		Flower s = flowerList.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return s.getId();
		case 1:
			return s.getName();
		case 2:
			return s.getPrice();
		default:
			return null;
		}
	}
}