package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Flower;

public class DBController {
	private Connection m_dbConnection = null;

	private Flower obj;

	List<Flower> listFlower = new ArrayList<Flower>();

	// DATABASE CONNECTION
	public DBController() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.Driver").newInstance();//localhost:3306
			m_dbConnection = DriverManager.getConnection ("jdbc:sqlserver://"+":"+3306+";FlowerShop=", "sa", "silvia1997");
//			Class.forName("com.mysql.jdbc.Driver").newInstance();
//			m_dbConnection = DriverManager
//					.getConnection("jdbc:mysql://localhost:3306/flowershop?user=root&password=silvia1997");
			System.out.println("conectat");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// READ
	public void showFlowers() {
		if (m_dbConnection == null)
			return;
		try {
			Statement stmt = m_dbConnection.createStatement();
			String sql = "SELECT idFlower, name, price FROM flower";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int idFlower = rs.getInt("idFlower");
				String name = rs.getString("name");
				int price = rs.getInt("price");

				System.out.println("idFlower: " + idFlower + ", name: " + name + ", price: " + price);
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	// INSERT
	public void insertFlower() {
		if (m_dbConnection == null)
			return;
		Statement stmt = null;
		try {
			stmt = m_dbConnection.createStatement();
			String sql = "INSERT INTO flower VALUES ('9', 'Poinsettia', '8')";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// UPDATE
	public void updateFlower(Flower flower) {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "UPDATE flower SET name = ?, price = ? WHERE idFlower = ?";
			stmt = m_dbConnection.prepareStatement(sql);
			stmt.setString(1, flower.getName());
			stmt.setInt(2, flower.getPrice());
			stmt.setInt(5, flower.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// DELETE
	public void deleteFlower(int idFlower) {
		if (m_dbConnection == null)
			return;
		Statement stmt = null;
		try {
			stmt = m_dbConnection.createStatement();
			String sql = "DELETE FROM flower WHERE idFlower = " + idFlower;
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// DELETE WITH STATEMENT
	public void deleteFlowerWithPreparedStatement(String value, String option) {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "DELETE FROM flower WHERE " + option + " = ?";
			stmt = m_dbConnection.prepareStatement(sql);
			stmt.setString(1, value);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
		}
	}

	// INSERT WITH STATEMENT
	public void insertFlowerWithPreparedStatement(int idFlower, String name, int price) {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO flower VALUES (?, ?, ?)";
			stmt = m_dbConnection.prepareStatement(sql);
			stmt.setInt(1, idFlower);
			stmt.setString(2, name);
			stmt.setInt(3, price);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
		}
	}

	// SORT AFTER PRICE
	public void sortFlowersAfterPrice() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT name, idFlower, price FROM flower WHERE price > 6 GROUP BY price ORDER BY name";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int idFlower = rs.getInt("idFlower");
				String name = rs.getString("name");
				int price = rs.getInt("price");

				System.out.println("idFlower: " + idFlower + ", name: " + name + ", price: " + price);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SORT AFTER TYPE
	public void flowerTypeOrdered() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT idFlower, name, idType, idColor FROM flower, type WHERE Type.idFlower = Flower.idFlower ORDER BY name";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int idFlower = rs.getInt("idFlower");
				String name = rs.getString("name");
				int idType = rs.getInt("idType");
				int idColor = rs.getInt("idColor");

				System.out.println(
						"idFlower: " + idFlower + ", name: " + name + ", idType: " + idType + ", idColor: " + idColor);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SORT AFTER LOW PRICE
	public void lowPriceFlowerOrdering() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT * FROM flower, type WHERE flower.idFlower = type.idFlower AND price < 10 ORDER BY price";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int idFlower = rs.getInt("idFlower");
				int price = rs.getInt("price");

				System.out.println("idFlower: " + idFlower + ", price: " + price);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// AVERAGE PRICE
	public void averageFlower() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT AVG(price) FROM flower";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int price = rs.getInt("price");

				System.out.println("price: " + price);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SUM PRICE
	public void sumFlowerConditioned() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT SUM(price) FROM flower WHERE price < 9";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int price = rs.getInt("price");

				System.out.println("price: " + price);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SUM CART REQUEST
	public void sumTotalRequestAfterCart() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT SUM(totalPrice) FROM cart, request, type WHERE cart.idRequest = request.idRequest AND request.idType = type.idType";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int totalPrice = rs.getInt("totalPrice");
				int idRequest = rs.getInt("idRequest");
				int idType = rs.getInt("idType");

				System.out.println("totalPrice: " + totalPrice);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SORT AFTER QUANTITY
	public void quantityModelOrdered() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT name, idModel, cantity, idRequest FROM arrangement, request WHERE request.idModel = arrangement.idModel AND idRequest < 5 ORDER BY cantity";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("name");
				int idModel = rs.getInt("idModel");
				int cantity = rs.getInt("cantity");
				int idRequest = rs.getInt("idRequest");

				System.out.println("name: " + name + ", idModel:" + idModel + ", cantity: " + cantity + ", idRequest: "
						+ idRequest);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// COUNT AFTER QUANTITY
	public void quantityArrangementCounter() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT COUNT(cantity) FROM arrangement WHERE name = 'Box'";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("name");
				int cantity = rs.getInt("cantity");

				System.out.println("cantity: " + cantity);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SORT AFTER AVERAGE
	public void averageQuantityBouquet() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT AVG(cantity), name, cantity, idModel FROM arrangement WHERE name = 'Bouquet' ORDER BY idModel";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("name");
				int cantity = rs.getInt("cantity");
				int idModel = rs.getInt("idModel");

				System.out.println("cantity: " + cantity);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// SUM TOTAL REQUEST
	public void totalRequestSum() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT SUM(totalPrice), idCart, idRequest FROM cart, request WHERE cart.idRequest = request.idRequest GROUP BY idCart";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int totalPrice = rs.getInt("totalPrice");
				int idCart = rs.getInt("idCart");
				int idRequest = rs.getInt("idRequest");

				System.out.println("totalPrice: " + totalPrice);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// ASCENDING ORDER AFTER PRICE
	public void ascendingOrderFlowerPrice() {
		if (m_dbConnection == null)
			return;
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT idFlower, name, price, idType FROM flower, type WHERE flower.idFlower = type.idFlower ORDER BY price asc";
			stmt = m_dbConnection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int idFlower = rs.getInt("idFlower");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				int idType = rs.getInt("idType");

				System.out.println(
						"idFlower: " + idFlower + ", name: " + name + ", price: " + price + ", idType" + idType);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {

			}
		}
	}

	// DATABASE CONNECTION
	public void closeConnection() {
		if (m_dbConnection != null) {
			try {
				m_dbConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			m_dbConnection = null;
		}
	}

	// LIST FLOWERS
	public List getFlowerDB(List<Flower> list) throws Exception {

		String querry = "insert into flower (id, name, price) values (?, ?, ?)";

		PreparedStatement preparedStmt = m_dbConnection.prepareStatement(querry);
		for (int i = 0; i < list.size(); i++) {
			preparedStmt.setInt(1, list.get(i).getId());
			preparedStmt.setString(2, list.get(i).getName());
			preparedStmt.setInt(3, list.get(i).getPrice());
			preparedStmt.executeUpdate();
		}
		preparedStmt.close();
		return list;
	}

	public List<Flower> getListFlower() {
		if (m_dbConnection == null)
			return null;
		try {
			//listFlower.clear();
			Statement stmt = m_dbConnection.createStatement();
			String sql = "SELECT id, name, price FROM flower";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				obj = new Flower(id, name, price);

				listFlower.add(obj);
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return listFlower;
	}
}