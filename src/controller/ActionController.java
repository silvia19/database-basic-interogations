package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.xml.sax.helpers.DefaultHandler;

import view.DeleteFlowerForm;
import view.MainForm;

public class ActionController implements ActionListener {

	MainForm m_form;

	DefaultHandler handler;

	public ActionController(MainForm mform) {
		m_form = mform;
	}

	public void actionPerformed(ActionEvent e) {
		JButton button1 = (JButton) e.getSource();
		if (button1.getText() == "Insert from database") {
			try {
				m_form.setlistFlowers(m_form.getDataBase().getListFlower());
			} catch (Exception e1) {

				e1.printStackTrace();
			}

		} else if (button1.getText() == "Insert") {
			m_form.insertRow();
		} else if (button1.getText() == "Delete specified input") {

			m_form.deleteFlowerForm = new DeleteFlowerForm(m_form);
			m_form.deleteFlowerForm.setVisible(true);
		} else if (button1.getText() == "Update") {
			m_form.updateSelectedRow();
		}
	}
}