package controller;

import javax.swing.JFrame;

import view.MainForm;

public class Main {
	public static void main(String[] args) throws Exception {
		DBController m_dbController = new DBController();
		MainForm mf = new MainForm();
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}