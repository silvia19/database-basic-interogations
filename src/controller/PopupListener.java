package controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class PopupListener extends MouseAdapter 
{
    private JPopupMenu popup;
    
    public PopupListener(JPopupMenu popup_menu) 
    {
    	popup = popup_menu;
    }

    public void mouseReleased(MouseEvent e) 
    {
        showPopup(e);
    }

    private void showPopup(MouseEvent e) 
    {
    	if (e.isPopupTrigger()) 
    	{
	    	 JTable source = (JTable)e.getSource();
	         int row = source.rowAtPoint(e.getPoint());
	         int column = source.columnAtPoint(e.getPoint());
	
	         if (! source.isRowSelected(row))
	         {
	             source.changeSelection(row, column, false, false);
	         }
	         
	         popup.show(e.getComponent(), e.getX(), e.getY());
    	}
    }
}