package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import controller.DBController;
import model.Flower;

public class FlowerForm extends JFrame {

	//private JTextField txtId;
	private JTextField txtName;
	//private JTextField txtPrice;
	private JButton button;
	JSpinner spinner;
	JSpinner spinnerId;
	private Flower obj = null;

	public FlowerForm(MainForm parent, Flower flower) {
		super();
		obj = flower;
		JPanel panel = new JPanel(null);
		add(panel);
		panel.setLayout(null);

		JLabel lbl = new JLabel("Id");
		lbl.setBounds(10, 10, 85, 20);
		panel.add(lbl);
		
		SpinnerModel value1 =  new SpinnerNumberModel(1, 1, 50, 1);
		spinnerId = new JSpinner(value1);   
		spinnerId.setBounds(110, 10, 150, 20);    
		this.add(spinnerId);
		

		lbl = new JLabel("Name");
		lbl.setBounds(10, 40, 85, 20);
		panel.add(lbl);

		txtName = new JTextField(25);
		txtName.setBounds(110, 40, 150, 20);
		panel.add(txtName);
		
		lbl = new JLabel("Price");
		lbl.setBounds(10, 100, 85, 20);
		panel.add(lbl);
		
		SpinnerModel value =  new SpinnerNumberModel(5, 1, 50, 1);
		spinner = new JSpinner(value);   
		spinner.setBounds(110, 130, 150, 20);    
		this.add(spinner);

		if (obj != null) {
			
			  spinnerId.setValue(obj.getId());
			  spinnerId.setEnabled(true);
			  txtName.setText(obj.getName());
			  spinner.setValue(obj.getPrice());
			  spinner.setEnabled(true);
		}
		button = new JButton("Send data");

		DBController m_dbController = new DBController();

		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (obj == null) {
					obj = new Flower();
					obj.setId(getId());
					obj.setName(getName());
					obj.setPrice(getPrice());

				} else {

					obj.setId(getId());
					obj.setName(getName());
					obj.setPrice(getPrice());
					parent.getDataBase().updateFlower(obj);
				}
				((DefaultTableModel) parent.getTableFlowers().getModel()).fireTableDataChanged();
				dispose();
			}
		});

		button.setBounds(340, 20, 100, 40);
		panel.add(button);

		add(panel);
		setSize(600, 300);
		setResizable(true);
		setLocationRelativeTo(parent);

		setVisible(true);
	}

	public int getId() {
		return (int) spinnerId.getValue();
	}

	public String getName() {
		return txtName.getText().trim();
	}

	public int getPrice() {
		return (int) spinner.getValue();
	}
}