package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controller.ActionController;
import controller.DBController;
import model.EditDataModel;
import model.Flower;
import controller.PopupListener;

public class MainForm extends JFrame {

	private JTextField txtDelete;
	private List<Flower> listFlowers;

	public FlowerForm flowerForm;
	public DeleteFlowerForm deleteFlowerForm;

	private static final long serialVersionUID = 1L;
	private EditDataModel tableModel;
	private FlowerForm editForm;
	JMenuBar menubar;
	JMenu menu;
	JMenuItem menuItem;
	private DBController controller;
	private ActionController actionController;

	public JPopupMenu t_Popup;
	private JTable tableFlowers = new JTable();

	public MainForm() {
		super("Flowers file");

		listFlowers = new ArrayList<Flower>();
		controller = new DBController();
		JPanel panel = new JPanel();
		panel.setLayout(null);
		JScrollPane frameScrPane = new JScrollPane(panel);
		this.add(frameScrPane);

		actionController = new ActionController(this);
		JButton button1 = new JButton();
		button1.setText("Insert from data base");
		button1.setBounds(30, 160, 190, 25);
		panel.add(button1);
		button1.addActionListener(actionController);

		setTableFlowers(new JTable());
		JScrollPane sp = new JScrollPane(getTableFlowers());
		sp.setBounds(235, 150, 600, 350);
		panel.add(sp);

		JButton buttonUpdate = new JButton();

		buttonUpdate.addActionListener(actionController);
		buttonUpdate.setText("Update");
		buttonUpdate.setBounds(30, 230, 190, 25);
		panel.add(buttonUpdate);

		JButton m_buttonInsert = new JButton();
		m_buttonInsert.setText("Insert");
		m_buttonInsert.setBounds(30, 310, 190, 25);
		panel.add(m_buttonInsert);
		m_buttonInsert.addActionListener(actionController);

		JButton m_buttonDeleteAll = new JButton();
		m_buttonDeleteAll.setText("Delete specified input");
		m_buttonDeleteAll.setBounds(30, 390, 190, 25);
		panel.add(m_buttonDeleteAll);
		m_buttonDeleteAll.addActionListener(actionController);

		t_Popup = new JPopupMenu();
		JMenuItem t_MenuItem = new JMenuItem("Update");
		t_MenuItem = new JMenuItem("Update");
		t_MenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				updateSelectedRow();
			}
		});

		t_Popup.add(t_MenuItem);

		t_MenuItem = new JMenuItem("Delete");
		t_MenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				deleteSelectedRow();
			}
		});

		t_Popup.add(t_MenuItem);
		MouseListener popupListener = new PopupListener(t_Popup);

		tableFlowers.setBounds(100, 40, 200, 300);
		tableModel = new EditDataModel(listFlowers);
		tableFlowers.setModel(tableModel);

		tableFlowers.setComponentPopupMenu(t_Popup);
		tableFlowers.addMouseListener(popupListener);

		add(panel);
		setSize(900, 800);
		setLocationRelativeTo(null);

		setVisible(true);
	}

	public void setTableFlowers(JTable tableFlowers) {
		this.tableFlowers = tableFlowers;
	}

	public List<Flower> getlistFlowers() {
		return listFlowers;
	}

	public void setlistFlowers(List<Flower> listFlowers) {
		//this.listFlowers.clear();
		this.listFlowers.addAll(listFlowers);
		tableModel.fireTableDataChanged();
	}

	public JTable getTableFlowers() {
		return tableFlowers;
	}

	public DBController getDataBase() {
		return controller;
	}

	public boolean ValidateTextField() {
		if (txtDelete.getText().isEmpty() || !(txtDelete.getText().matches("[0-9]+"))) {
			return false;
		}
		return true;
	}

	public void ResetTextField() {
		txtDelete.setText("");
	}

	public String GetTextField() {
		return txtDelete.getText();
	}

	public void deleteSelectedRow() {
		int index = tableFlowers.getSelectedRow();
		System.out.println(index);
		if (index >= 0 && index < listFlowers.size()) {
			Flower flower = listFlowers.get(index);
			int id = flower.getId();
			tableModel.removeById(id);
			controller.deleteFlower(id);
		}
	}

	public void insertRow() {
		editForm = new FlowerForm(this, null);
		editForm.setVisible(true);
		editForm.setBounds(10, 40, 400, 300);
	}

	public void updateSelectedRow() {
		int index = tableFlowers.getSelectedRow();
		System.out.println(index);
		if (tableFlowers.getSelectionModel().isSelectionEmpty()) {
			JOptionPane.showMessageDialog(this, "Please select a row first.", "Update error",
					JOptionPane.ERROR_MESSAGE);
		} else {
			Flower o_flower = listFlowers.get(index);
			editForm = new FlowerForm(this, o_flower);
			editForm.setVisible(true);
			editForm.setBounds(10, 40, 400, 300);
		}
	}
}