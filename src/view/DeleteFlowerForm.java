package view;

import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.DBController;
import model.EditFlowers;

@SuppressWarnings("serial")
public class DeleteFlowerForm extends JDialog {

	private String value;
	private JTextField txtName;
	private JButton button;
	private List atributeFlower;

	public DeleteFlowerForm(MainForm parent) {

		super();

		JPanel panel = new JPanel(null);

		String options[] = { "id", "name", "price"};
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(options);
		JList<String> jlist = new JList<>(model);
		jlist.setBounds(160, 60, 200, 100);
		panel.add(jlist);

		JLabel lbName = new JLabel("Value: ");
		lbName.setBounds(20, 20, 100, 40);
		panel.add(lbName);

		txtName = new JTextField(20);
		txtName.setBounds(130, 20, 200, 30);
		panel.add(txtName);

		button = new JButton("Delete information:");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DBController m_dbController = new DBController();
				value = getName();

				String option = jlist.getSelectedValue().toString();
				parent.getDataBase().deleteFlowerWithPreparedStatement(value, option);

				if (option == "id") {
					for (int i = 0; i < parent.getlistFlowers().size(); i++) {
						if (parent.getlistFlowers().get(i).getId() == Integer.parseInt(value)) {
							parent.getlistFlowers().remove(i);
						}
					}
				} else if (option == "name") {
					for (int i = 0; i < parent.getlistFlowers().size(); i++) {
						if (parent.getlistFlowers().get(i).getName().equalsIgnoreCase(value)) {
							parent.getlistFlowers().remove(i);
						}
					}
				} else if (option == "price") {
					for (int i = 0; i < parent.getlistFlowers().size(); i++) {
						if (parent.getlistFlowers().get(i).getPrice() == Integer.parseInt(value)) {
							parent.getlistFlowers().remove(i);
						}
					}
				} 
				EditFlowers model = new EditFlowers(parent.getlistFlowers());
				parent.getTableFlowers().setModel(model);
				dispose();
			}
		});

		button.setBounds(340, 20, 100, 40);
		panel.add(button);

		add(panel);
		setSize(600, 300);
		setResizable(true);
		setLocationRelativeTo(parent);

		setVisible(false);
	}

	public String getName() {
		return txtName.getText().trim();
	}
}
